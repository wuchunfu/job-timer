const timer = require('../timer')

function test1() {

	let t1 = {
		name : 'timer 1',
		cycle : 5000,
		fun () {
			console.log(new Date().format('yyyy-MM-dd HH:mm:ss') + ' time1 .....')
		}
	}

	let t2 = {
		name : 'time2',
		cycle : 3000,
		begin : new Date(new Date().getTime() + 7000),
		args: 'acdqwefasdfqw',
		fun (args) {
			console.log(new Date().format('yyyy-MM-dd HH:mm:ss') + ' timer2.......' + args)
		} 
	}


	let t3 = {
		name : 'time3',
		cycle : 10000,
		begin : new Date('2014-04-03 12:30:00'),
		args: new Date('2018-04-03 12:30:00'),
		fun (args) {
			console.log(new Date().format('yyyy-MM-dd HH:mm:ss') + ' timer3.......' + args)
		} 
	}

	timer.addTask(t1)
	timer.addTask(t2)
	timer.addTask(t3)
}

function test2() {
	let a = 'ssuccee'
	const f = new Promise((suc, fail) => {
		timer.addTask({
			name: 'test',
			cycle: 5000,
			fun () {
				console.log(a)
				a += 't'
				if (a.length === 10) {
					suc(a)
				}
			}
		})
	})

	f.then(x => {
		console.log('suc:' + x)
	})
}

test2()