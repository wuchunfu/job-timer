module.exports = {
	getBatches: {
    url: '/batches',
    method: 'get',
    description: '获取 batches'
  },
	postBatch: {
    url: '/batches',
    method: 'post',
    description: '提交 batches'
  },
	getBatch: {
    url: '/batches/{}',
    method: 'get',
    description: '获取 batche 信息'
  },
	getBatchState: {
    url: '/batches/{}/state',
    method: 'get',
    description: '获取 batche 状态'
  },
	delBatch: {
    url: '/batches/{}',
    method: 'delete',
    description: '删除 batche'
  },
	getBatchLog: {
    url: '/batches/{}/log',
    method: 'get',
    description: '获取 batche 日志'
  }
}